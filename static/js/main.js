/*global jQuery, document, window */

(function (g, $) {
    'use strict';
    /**
     * Глобальные скрипты для всего сайта
    **/


    /* --- Инициализация скриптов ---*/
        $('input:not(.no_styler), select').styler();  // formstyler
        setTimeout(function() {  
            $('input, select').styler();  
        }, 100)    

        // fancybox defaults
        $.extend($.fancybox.defaults, {
            nextEffect: "fade",
            prevEffect: "fade",
            nextSpeed: "slow",
            prevSpeed: "slow",
        });

        //   Открывать все изображения в fancybox 
        $("a[href$='.jpg'], a[href$='.jpeg'], a[href$='.png'],a[href$='.gif']").attr('rel', 'gallery').fancybox({
            'padding': 0,
            'transitionIn' : 'none',
            'transitionOut' : 'none',
            helpers : {
                title: null,
                overlay : {
                    css : {
                        'background' : 'rgba(0, 0, 0, 0.6)'
                    }
                }
            },
        });

        jQuery.validator.setDefaults({
            success: "valid",

            invalidHandler: function() {  
                setTimeout(function() {  
                    $("input[type='radio'], #fancybox_reg input[type='checkbox']").trigger('refresh');  
                }, 1)  
            },
            
            errorPlacement: function(error, element) {
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.is("select")  ) {
                    if(element.attr("type") == "radio")
                        error.insertAfter(element.parent().parent().next());
                    else{
                        if(element.is("select") )
                            error.insertAfter(element.parent());
                        else    
                            error.insertAfter(element.parent().parent());
                    }
                } else {
                    error.insertAfter(element);
                }

            },
        });

    /* --- END Инициализация скриптов ---*/

    /* --- Всплывающая подсказка ---*/
        function Common() {
            this.initPopovers = function () {
                var popovers = $('.popover-hint');
                popovers.each(function(i, e) {
                    var $e = $(e), tri = $e.children('.popover-hint__tri');
                    if (!tri.length) {
                        tri = document.createElement('div');
                        tri.className = 'popover-hint__tri';
                        $e.append(tri);
                    }
                });
                popovers.on('click', function () {
                    var $_this = $(this);
                    $_this.toggleClass('popover-hint_show');
                });
                $(document).on('mousedown touchstart', function (e) {
                    if (!popovers.has(e.target).length) {
                        popovers.removeClass('popover-hint_show');
                    }
                });
            };
            this.init = function () {
                this.initPopovers();
            };
        }

        var common = new Common();
        $(document).ready(function () {
            common.init();
        });
    /* --- END Всплывающая подсказка ---*/


// и т.д.


})(window, jQuery);
