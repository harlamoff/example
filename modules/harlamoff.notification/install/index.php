<?
global $MESS;
 
if(class_exists("notification")) return;
 
Class Harlamoff_notification extends CModule{
    var $MODULE_ID = "harlamoff.notification";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";
 
    function Harlamoff_notification(){
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)){
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }else{
            $this->MODULE_VERSION = FORM_VERSION;
            $this->MODULE_VERSION_DATE = FORM_VERSION_DATE;
        }

        $this->MODULE_NAME = "Notification – модуль Push & Pull";
        $this->MODULE_DESCRIPTION = 'Счетчик уведомлений';
    }
 
    function DoInstall(){
        global $DB, $DOCUMENT_ROOT, $APPLICATION, $step, $errors, $public_dir;
        $this->InstallFiles();
        RegisterModule("harlamoff.notification");      
    }
     
    function InstallFiles($arParams = array()){
        return true;
    }
    
    function UnInstallFiles(){
        return true;
    }
     
    function DoUninstall(){
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        UnRegisterModule("harlamoff.notification");
    }
}
?>