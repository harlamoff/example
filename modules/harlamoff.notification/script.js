$(function(){
    BX.ready(function(){
        // Кастомное событие onPullEvent-harlamoff.notification
        BX.addCustomEvent("onPullEvent-harlamoff.notification", function(command,params) {
            // console.log(command,params);
            if (command == 'notificantion_message' ){
                messageInc();
            }else{
                alert("Ошибка неверная команда.");
            }
        });
    });

    // Общее уведомление в шапке.
    function generalInc(){
        var general = 0; 
        general = parseInt($(".header__top__btn_profile__new-event").text());
        if(general > 0)
            $("#notification_general").text(general+1);
        else
            $("#notification_general").addClass("header__top__btn_profile__new-event").text(1);
        soundPlay();
    }

    // Уведомления в Меню
    function messageInc(){
        var message = 0; 
        message = parseInt($("#notification_message").text());
        if(message > 0)
            $("#notification_message").text(message+1);
        else
            $("#notification_message").addClass("cabinet__menu-counter_highlighted").text(1);
        generalInc();
    }

    function soundPlay() {
        var audio = new Audio(); // Создаём новый элемент Audio
        audio.src = '/local/modules/harlamoff.notification/ring.mp3'; // Указываем путь к звуку "клика"
        audio.autoplay = true; // Автоматически запускаем
    }

});
