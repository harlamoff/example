<?
if(!CModule::IncludeModule('pull')) exit()
if(!CModule::IncludeModule("iblock")) exit();

// Регистрирует обработчик события.
RegisterModuleDependences("pull", "OnGetDependentModule", "harlamoff.notification", "CnotificationPullSchema", "OnGetDependentModule" );
class CnotificationPullSchema{
    public static function OnGetDependentModule(){
		return Array(
			'MODULE_ID' => "harlamoff.notification",
			'USE' => Array("PUBLIC_SECTION")
		);
    }
}


/**
* Выводит счетчик уведомлений и оповещает пользователя мелодией в режиме реального времени. 
* Получает кол-во: сообщений, событий, отзывов, объявлений, тендеров, проектов.
* Работает через Push & Pull
* 
* Пример http://xn--e1agleejfol4ib.xn--p1ai/tender/perekryt-kryshu_1102/
*/
class Notification{

	/**
	 * Получить кол-во сообщений
	 * @return int
	 */
	public function GetNotificationMsg($usr){
		$arFilter = array(
			"IBLOCK_ID"=>11,
			"ACTIVE"=>"Y",
			"ACTIVE_DATE"=>"Y",
			"PROPERTY_TO"=>$usr,
			"!PROPERTY_VIEWED_VALUE"=>"Y"
		);
		$arSelect = Array("IBLOCK_ID", "ID"); 
		$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
		$count = $items->SelectedRowsCount();

		return $count;
	}

	/**
	 * Получить кол-во событий
	 * @return int
	 */
	public function GetNotificationEvent($usr){
		$arFilter = array(
			"IBLOCK_ID"=>15, 
			"ACTIVE"=>"Y", 
			"ACTIVE_DATE"=>"Y", 
			"PROPERTY_TO"=>$usr, 
			"!PROPERTY_VIEWED_VALUE"=>"Y"
		);
		$arSelect = Array("IBLOCK_ID", "ID"); 
		$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
		$count = $items->SelectedRowsCount();

		return $count;
	}

	/**
	 * Получить кол-во уведомлений отзывов
	 * @return int
	 */
	public function GetNotificationReview($usr){
		$arFilter = array(
			"IBLOCK_ID"=>12, 
			"ACTIVE"=>"Y", 
			"ACTIVE_DATE"=>"Y", 
			"PROPERTY_MASTER"=>$usr, 
			"!PROPERTY_VIEWED_VALUE"=>"Y"
		);
		$arSelect = array("IBLOCK_ID", "ID" ); 
		$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
		$count = $items->SelectedRowsCount();

		return $count;
	}

	/**
	 * Получить кол-во объявлений
	 * @return int
	 */
	public function GetNotificationAdword($usr){
		$arFilter = array(
			"IBLOCK_ID"=>13, 
			"ACTIVE"=>"Y", 
			"ACTIVE_DATE"=>"Y", 
			"PROPERTY_USER"=>$usr
		);
		$arSelect = array("IBLOCK_ID", "ID" ); 
		$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
		$count = $items->SelectedRowsCount();

		return $count;
	}


	/**
	 * Получить кол-во тендеров
	 * @return int
	 */
	public function GetNotificationTender($usr){
		$arFilter = array(
			"IBLOCK_ID"=>7, 
			"ACTIVE"=>"Y", 
			"ACTIVE_DATE"=>"Y", 
			"PROPERTY_USER"=>$usr, 
			"PROPERTY_STATUS"=> 28,
			array(
				"LOGIC" => "OR",
				array("PROPERTY_MASTER" => CUser::GetID() ),
				array("PROPERTY_USER" => CUser::GetID() ),
			),
		);
		$arSelect = array("IBLOCK_ID", "ID" ); 
		$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
		$count = $items->SelectedRowsCount();

		return $count;
	}

	/**
	 * Получить кол-во текущих проектов
	 * @return int
	 */
	public function GetNotificationProject($usr){
		$arFilter = array(
			"IBLOCK_ID"=>7, 
			"ACTIVE"=>"Y", 
			"ACTIVE_DATE"=>"Y", 
			"!PROPERTY_STATUS"=> 28,
			array(
				"LOGIC" => "OR",
				array("PROPERTY_MASTER" => CUser::GetID() ),
				array("PROPERTY_USER" => CUser::GetID() ),
			),
		);
		$arSelect = Array("IBLOCK_ID", "ID"); 
		$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
		$count = $items->SelectedRowsCount();

		return $count;
	}	
}

?>