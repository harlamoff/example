<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>


    <div class="list-orders_with">

        <figure class="list-orders__item">
            <div class="list-orders__location"><?=$arResult["CITY"]["CITY_NAME"]?></div>
            <h2 class="list-orders__item-header h2">
                <?=$arResult["NAME"]?>
            </h2>

            <div class="list-orders__text_full">
                <?=$arResult["DETAIL_TEXT"]?>
            </div>
            
            <div class="list-orders__category">Категория: <a class="last-order__category-link" href="/tender/?s=<?=$arResult["CAT"]["ID"]?>"><?=$arResult["CAT"]["NAME"]?></a></div>

            <p><b>Расположение объекта:</b></p>
            <p>
                <?=$arResult["CITY"]["CITY_NAME"]?>
                &nbsp; &nbsp; &nbsp; 
                <?if(strlen($arResult["PROPERTIES"]["RAION"]["VALUE"]) > 1 ){?>
                    Район: <?=$arResult["PROPERTIES"]["RAION"]["VALUE"]?>
                <?}?>
            </p>

            <?if(strlen($arResult["PROPERTIES"]["TIME"]["VALUE"]) > 1 ){?>
                <p><b>Сроки:</b> <?=$arResult["PROPERTIES"]["TIME"]["VALUE"]?></p>
            <?}?>            
            
            <?if(strlen($arResult["PROPERTIES"]["PHONE"]["VALUE"]) > 1 ){?>
                <p><b>Телефон для связи:</b> <?=$arResult["PROPERTIES"]["PHONE"]["VALUE"]?></p>
            <?}?>    

            <?if( $arResult["PROPERTIES"]["FILES"]["VALUE"]){?>
                <hr class="list-orders__hr_pad">
                <p><b>Прикрепленные файлы:</b></p>
                <div class="doc">
                    <?foreach($arResult["PROPERTIES"]["FILES"]["VALUE"] as $key => $arItem) {?>
                       <? $file = CFile::GetByID($arItem)->fetch();
                       $ext = pathinfo($file["FILE_NAME"],PATHINFO_EXTENSION);
                       $ext = strtolower($ext);
                       ?>
                       <a href="<?=CFile::GetPath($arItem)?>" class="doc__<?=$ext?> doc__item" download><?=$file["ORIGINAL_NAME"]?></a>
                    <?}?>
                </div>
            <?}?>    
         
            <br>
            <br>
            <?if($arResult["PROPERTIES"]["PRO"]["VALUE"] == 1 ){?>
                <div class="list-orders__pro">Только для PRO</div>
            <?}?>   

            <div class="list-orders__date"><?=FormatDate("x", MakeTimeStamp($arResult["ACTIVE_FROM"]))?></div>

            <?if($arResult["PROPERTIES"]["REPEAT"]["VALUE"] > 0 ){?>
                <div class="list-orders__repeat">Размещен повторно</div>
            <?}?>
        </figure>

        <?if(!empty($arResult["PROPERTIES"]["DEALS"]["VALUE"][0]["USER"])){
            $count_deals = count($arResult["PROPERTIES"]["DEALS"]["VALUE"]);
        }else{
            $count_deals = 0;
        }?>

        <?if($count_deals > 0){?>
            <div class="list-orders__no-offers_active list-orders__no-offers list-orders__no-offers_pad">Предложений: <?=$count_deals?></div>        
        <?}else{?>
            <div class="list-orders__no-offers">Нет предложений</div>
        <?}?>
    </div>

    <div class="tender-comments">
        <?if($arResult["PROPERTIES"]["STATUS"]["VALUE_ENUM_ID"] != 22){?>
            <?if($USER->IsAuthorized()){?>
                <!-- Когда юзер авторизирован -->
                <? 
                    // Если только для PRO
                    if( $arResult["thisUser"]["add_deals"] == 1 ){?>
                        <?// Получаем уменьщеную аватарку пользователя
                        $avatar_86 = CFile::ResizeImageGet($arResult["thisUser"]['PERSONAL_PHOTO'], array("width" => 86, "height" => 86), BX_RESIZE_IMAGE_EXACT, true) ;
                        if($avatar_86["src"] == ""){ // если нет изображения, выводим 
                            $avatar_86["src"] = "/static/img/icons/avatar@2.png";
                        }?>
                        <div class="tender-comments__item__trim"><img class="tender-comments__item__photo" src="<?=$avatar_86["src"]?>" alt=""></div>
                        <h2 class="tender-comments__h2">Сделать свое предложение:</h2>
                        
                        <form id="tender-comments__add" class="tender-comments__add" method="post" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data">
                            <?=bitrix_sessid_post()?>
                            <div class="tender-comments__add__wrap">
                                <div class="tender-comments__add__col">
                                    <div class="tender-comments__add__label">Цена: <span class="g-star">*</span></div>
                                </div>
                                <div class="tender-comments__add__col_input">
                                    <input name="price" class="tender-comments__add__input" type="text" placeholder="Например: 3000 руб за м2">
                                </div>
                            </div>

                            <div class="tender-comments__add__wrap">
                                <div class="tender-comments__add__col">
                                    <div class="tender-comments__add__label">Стоимость выезда на объект: <span class="g-star">*</span></div>
                                </div>
                                <div class="tender-comments__add__col_input">
                                    <input name="price_road" class="tender-comments__add__input" type="text" placeholder="Например: 3000 руб">
                                </div>
                            </div>

                            <div class="tender-comments__add__wrap">
                                <div class="tender-comments__add__col">
                                    <div class="tender-comments__add__label">Предоплата за услуги: <span class="g-star">*</span></div>
                                </div>
                                <div class="tender-comments__add__col_input">
                                    <label class="tender-comments__add__radio">
                                        <input type="radio" name="prepay_service" value="Y">
                                        Да
                                   </label>
                                    <label class="tender-comments__add__radio">
                                        <input type="radio" name="prepay_service" value="N" checked>
                                        Не обязательна
                                   </label>
                                </div>
                            </div>

                            <div class="tender-comments__add__wrap">
                                <div class="tender-comments__add__col">
                                    <div class="tender-comments__add__label">Предоплата за материалы: <span class="g-star">*</span></div>
                                </div>
                                <div class="tender-comments__add__col_input">
                                    <label class="tender-comments__add__radio">
                                        <input type="radio" name="prepay_materials" value="Y">
                                        Да
                                   </label>
                                    <label class="tender-comments__add__radio" >
                                        <input type="radio" name="prepay_materials" value="N" checked>
                                        Не обязательна
                                   </label>
                                </div>
                            </div>
                                                                            
                            <div class="tender-comments__add__wrap">
                                <div class="tender-comments__add__col">
                                    <div class="tender-comments__add__label">
                                    Сообщение заказчику: 
                                    <span class="g-star">*</span>
                                    <div class="tender-comments__add__label_max">Не более 1000 символов</div>
                                </div>
                                </div>
                                <div class="tender-comments__add__col_input">
                                    <textarea name="text" class="tender-comments__add__textarea" placeholder="Например: до 18.06.17 / 3 месяца  / не важно"></textarea> 
                                </div>
                            </div>

                            <button type="submit" class="btn btn_7 tender-comments__add__submit">Отправить</button>
                        </form>
                    <?}else{?>
                        <?// Если только для PRO
                        if( $arResult["thisUser"]["add_deals"] == 0 ){?>
                            <div>Оставлять предложения могут только пользователи с PRO аккаунтом.</div>
                        <?}?>

                        <?// Уже оставлял предложение
                        if( $arResult["thisUser"]["add_deals"] == 4 ){?>
                            <div>Вы уже сделали предложение.</div>
                        <?}?>

                        <?// Вы исчерпали лимит предложений за сегодня.
                        if( $arResult["thisUser"]["add_deals"] == 5 ){?>
                            <div>Вы исчерпали лимит предложений за сегодня.</div>
                        <?}?>

                    <?}?>

            <?}else{?>
                Оставлять предложения могут только авторизованные пользователи.
            <?}?>
        <?}?>

        <h2 class="tender-comments__h2">Предожения мастеров:</h2>
        <?if(count($arResult["NAV"]["DEALS"]) > 0 ){?>
                
            <?foreach ($arResult["NAV"]["DEALS"] as $arItem) {?>
                <? // для PRO или автора заказа или автора предложения
                if( $arResult["thisUser"]["UF_PRO"] == 1 || $arResult["thisUser"]["add_deals"] == 3 || $arItem["USER"]["ID"] == $USER->GetID() ){?>
                    <figure class="tender-comments__item">
                        <div class="tender-comments__item__col">
                            <div class="tender-comments__item__trim">
                                <?// Получаем уменьщеную аватарку пользователя
                                $avatar_deals_86 = CFile::ResizeImageGet($arItem["USER"]['PERSONAL_PHOTO'], array("width" => 86, "height" => 86), BX_RESIZE_IMAGE_EXACT, true) ;
                                if($avatar_deals_86["src"] == ""){ // если нет изображения, выводим 
                                    $avatar_deals_86["src"] = "/static/img/icons/avatar@2.png";
                                }?>                     
                                <img class="tender-comments__item__photo" src="<?=$avatar_deals_86["src"]?>" alt="<?=$arItem["USER"]["TITLE"]?>">
                            </div>
                        </div>
                        <div class="tender-comments__item__col_2">
                            <div class="tender-comments__item__wrap-user-data">
                                <?if( $arItem["USER"]["UF_PRO"] == 1 ){?>
                                    <div class="tender-comments__item__pro">PRO</div>
                                <?}?>

                                <a href="/user/<?=$arItem["USER"]["ID"]?>/" class="tender-comments__item__user-name"><?=$arItem["USER"]["TITLE"]?></a>
                                <div class="g-clr"></div>
                                <div class="tender-comments__item__date"><?=FormatDate("x", MakeTimeStamp($arItem["DATE"]))?></div>
                                <p><b>Цена:</b> <?=$arItem["PRICE"]?></p>
                                <p><b>Стоимость выезда на объект:</b> <?=$arItem["PRICE_ROAD"]?></p>
                                <p><b>Предоплата за услуги:</b> <?if($arItem["PREPAY_SERVICE"]=="Y"){?>Да<?}else{?>Нет<?}?></p>
                                <p><b>Предоплата за материалы:</b> <?if($arItem["PREPAY_MATERIALS"]=="Y"){?>Да<?}else{?>Нет<?}?></p>
                                <p><?=$arItem["TEXT"]?></p>
                            </div>
                    
                            <a href="/user/messages/detail/<?=$arItem["USER"]["ID"]?>/" class="btn btn_9 tender-comments__item__btn ">
                                <i class="icon_clouds"></i>
                                Написать сообщение
                            </a>

                            <a href="/user/services/<?=$arItem["USER"]["ID"]?>/" class="tender-comments__item__link g-hover">
                                <i class="icon_service_green"></i>
                                <span>Услуги и цены</span>
                            </a>
                            <a href="/user/gallery/<?=$arItem["USER"]["ID"]?>/" class="tender-comments__item__link g-hover">
                                <i class="icon_camera_green"></i>
                                <span>Галерея</span>
                            </a>
                            <a href="/user/reviews/<?=$arItem["USER"]["ID"]?>/" class="tender-comments__item__link g-hover">
                                <i class="icon_heart_green"></i>
                                <span>Отзывы</span>
                            </a>                
                        </div>
                    </figure>
                <?}?>
            <?}?>

            <? // для не PRO и автора заказа
            if( $arResult["thisUser"]["UF_PRO"] != 1 && $arResult["thisUser"]["add_deals"] != 3 ){?>
                <p>Вы не можете просматривать предложения других мастеров. Это доступно только для <a href="/pro/" target="_blank">PRO</a> аккаунтов.</p>
            <?}?>    

        <?}else{?>
            <p>Нет предложений.</p>
        <?}?>

    </div>

    <!-- Постраничная навигация -->
    <?=$arResult["NAV"]["NAV_STRING"]?>
