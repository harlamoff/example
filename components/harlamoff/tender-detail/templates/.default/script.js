$(function(){
    // Валидация формы
    $("#tender-comments__add").validate({
        rules: {
            price: {
                required: true,
                number: true,
                maxlength: 11,                            
            },

            price_road: {
                required: true,
                number: true,
                maxlength: 11, 
            },   

            prepayment: {
                required: true,
            },  

            prepayment_material: {
                required: true,
            },  
            
            text: {
                required: true,
                minlength: 20,
                maxlength: 1000, 
            },   

        },

    });

    // Скачивание, а не открытие в fancybox картинок
    $(".doc__item").on("click", function(e){
        e.stopPropagation();
    });

});