<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Выбор инфоблока
if(!CModule::IncludeModule("iblock")) exit();
$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(
	Array("SORT"=>"ASC"),
	Array(
		"SITE_ID"=>$_REQUEST["site"],
		"TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")
	)
);

while($arRes = $db_iblock->Fetch()) {
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => "IBLOCK_TYPE",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "",
			"REFRESH" => "Y",
		),

		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "IBLOCK_ID",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),

		"CNT_NAV" => Array(
			"PARENT" => "BASE",
			"NAME" => "Кол-во предложений на странице",
			"TYPE" => "STRING",
			"VALUES" => "",
			"DEFAULT" => "10",
		),

		"URL" => Array(
			"PARENT" => "BASE",
			"NAME" => "URL ",
			"TYPE" => "STRING",
			"VALUES" => "",
			"DEFAULT" => "",
		),

		"DEALS_DEY" => Array(
			"PARENT" => "BASE",
			"NAME" => "Кол-во предложений за 24ч",
			"TYPE" => "STRING",
			"VALUES" => "",
			"DEFAULT" => "2",
		),

		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),

	),
);
?>