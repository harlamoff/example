<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

if(!CModule::IncludeModule("iblock")) exit();
if(!CModule::IncludeModule("sale")) exit();

/**
* Выбирает необходимые тендеры по url, 
* добавляет предложения мастеров, 
* выводит предложения.
* 
* Пример http://xn--e1agleejfol4ib.xn--p1ai/tender/perekryt-kryshu_1102/
*/
class TenderDetail extends CBitrixComponent{

	/**
	 * получение текущего id тендера из url
	 * @return int
	 */
	private function getThisUrl(){
		$arVariables = array();
		CComponentEngine::ParseComponentPath(
		   $this->arParams["URL"],
		   array('#ELEMENT_CODE#/', '#ELEMENT_CODE#'),
		   $arVariables
		);
		$arVariables["ELEMENT_CODE"] = htmlspecialcharsEx($arVariables["ELEMENT_CODE"]);
		// Получаем отесекая все до последнего -
		$id = intval( preg_replace("/.*?\_/", '', $arVariables["ELEMENT_CODE"]) );
		if($id > 0){
			return $id;
		}else{
			LocalRedirect($this->arParams["URL"]);
			exit;
		}
	}

	/**
	 * если url не совпадает с правильным то делаем редирект.
	 */
	private function restoreUrl($code, $id){

		$arVariables = array();
		CComponentEngine::ParseComponentPath(
		   $this->arParams["URL"],
		   array('#ELEMENT_CODE#/', '#ELEMENT_CODE#'),
		   $arVariables
		);
		$arVariables["ELEMENT_CODE"] = htmlspecialcharsEx($arVariables["ELEMENT_CODE"]);
		$new_url = $code."_".$id;

		if($arVariables["ELEMENT_CODE"] != $new_url ){
			// если url не совпадает с правильным то делаем редирект.
			LocalRedirect($this->arParams["URL"].$new_url."/");
			exit;
		}
	}

	/**
	 * Получаем разом необходимые города - экономим запросы
	 * @return array;
	 */
	private function getCity($arCityListId){
		$res = CSaleLocation::GetList(Array("NAME_LANG"=>"ASC"), Array("ID"=>$arCityListId), LANGUAGE_ID) ;
		while($val = $res->Fetch()){
		   $city = $val; 
		}

		return $city;
	}

	/**
	 * Получаем разом необходимы категории - экономим запросы.
	 * @return array;
	 */
	private function getCat($arCatListId){

		$res = CIBlockSection::GetList(Array(), Array("ID"=>$arCatListId, "ACTIVE"=>"Y"), false, array("ID", "NAME")) ;
		while($val = $res->Fetch()){
		   $cat = $val; 
		}

		return $cat;
	}

	/**
	 * Получаем пользователя
	 * @return array;
	 */
	private function getUser($id){
		$user = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$id),
			array(
				"FIELDS"=>array("ID", "PERSONAL_PHOTO", "TITLE"),
				"SELECT"=>array("UF_PRO"),
			)
		)->fetch();

		return $user;
	}

	/**
	 * Добавляем новое предложение
	 * @return array;
	 */
	private function addDeal($arResult){
		// Проверка пришла ли форма
		if( check_bitrix_sessid() && CUser::IsAuthorized() && ( $arResult["thisUser"]["add_deals"] != 3 || $arResult["thisUser"]["add_deals"] != 4 )){
			//добавления нового предложения в ИБ
			$res_table = $arResult["PROPERTIES"]["DEALS"]["VALUE"];
			$res_table[] = array(
			    "USER" => CUser::GetID(),
			    "DATE" => date(CDatabase::DateFormatToPHP(CSite::GetDateFormat("FULL")), time()),
			    "PRICE" => substr(htmlspecialcharsEx($_POST["price"]), 0, 11),
			    "PRICE_ROAD" =>  substr(htmlspecialcharsEx($_POST["price_road"]), 0, 11),
			    "PREPAY_SERVICE" => htmlspecialcharsEx($_POST["prepay_service"]),
			    "PREPAY_MATERIALS" => htmlspecialcharsEx($_POST["prepay_materials"]),
			    "TEXT" =>  substr(htmlspecialcharsEx($_POST["text"]), 0, 1000),
			);

			$arVariables = array();
			CComponentEngine::ParseComponentPath(
			   $this->arParams["URL"],
			   array('#ELEMENT_CODE#/', '#ELEMENT_CODE#'),
			   $arVariables
			);
			$arVariables["ELEMENT_CODE"] = htmlspecialcharsEx($arVariables["ELEMENT_CODE"]);

			CIBlockElement::SetPropertyValueCode($arResult["ID"], "DEALS", $res_table );

			// Поля текущего юзера
    		$rsUser = CUser::GetByID(CUser::GetID())->Fetch();
    		// Отравка события пользователю
			$APPLICATION->IncludeComponent(
				"harlamoff:events-new",
				"",
				Array(
					"COMPONENT_TEMPLATE" => ".default",
					"IBLOCK_TYPE" => "USER",
					"IBLOCK_ID" => "15",
					"MESSAGE" => '
				        <p>Мастер <a href="/user/'.$rsUser["ID"].'/">'.$rsUser["TITLE"].'</a> предложил выполнить работу 
				        <br><br>  
				        <a href="'.$this->arParams["URL"].$arResult["ID"].'/" class="btn btn_7">Открыть все предложения мастеров</a>

					',
					"FROM" => 'Мастер '.$rsUser["TITLE"].' предложил выполнить работу',
					"TO" => $arResult["PROPERTY_USER_VALUE"],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600"
				)
			);

			// Готово. делаем редирект			
			LocalRedirect($this->arParams["URL"].$arVariables["ELEMENT_CODE"]."/");
			exit;

			return $arRes;
		}
	}

	/**
	 * Разбива на постраничку комментариев (предложений)
	 * @return array;
	 */
	private function getNav($res){
	    function cmp_DATE_DESC($a, $b) {
	        if ($a["DATE"] == $b["DATE"]) {
	        	return 0;
	        }
	        return ($a["DATE"] > $b["DATE"]) ? -1 : 1;
	    }
		usort($res, "cmp_DATE_DESC");

		$rs_ObjectList = new CDBResult;
		$rs_ObjectList->InitFromArray($res);
		$rs_ObjectList->NavStart($this->arParams["CNT_NAV"], false);
		$arResult["NAV_STRING"] = $rs_ObjectList->GetPageNavString("", "");
		$arResult["PAGE_START"] = $rs_ObjectList->SelectedRowsCount() - ($rs_ObjectList->NavPageNomer - 1) * $rs_ObjectList->NavPageSize;
		$key=0;
		while($ar_Field = $rs_ObjectList->Fetch()){
			$arResult['DEALS'][$key] = $ar_Field;
			// Получаем поля пользователя. Одним запросом не получается т.к нельзя выбрать юзеров через массив ID
		    $arResult['DEALS'][$key]["USER"] = $this->getUser($ar_Field["USER"]);	
		   
		    $key++;
		}

		return $arResult;
	}

	/**
	 * Получаем список услуги  
	 * @return array;
	 */
	private function getTender(){

		$arFilter = array(
			"IBLOCK_ID"=>$this->arParams["IBLOCK_ID"], 
			"ID"=>$this->getThisUrl(), 
			"ACTIVE"=>"Y", 
			"ACTIVE_DATE"=>"Y"
		);
		$arSelect = Array(
			"IBLOCK_ID", 
			"ID", 
			"NAME", 
			"DETAIL_TEXT", 
			"ACTIVE_FROM", 
			"CODE", 
			"PROPERTY_USER"
		); 

		$items = CIBlockElement::GetList( array("id"=>"desc"), $arFilter, false, false, $arSelect );

		while($arEl = $items->GetNextElement()):
		    $el = $arEl->GetFields();
			$el["PROPERTIES"]= $arEl->GetProperties();  
		    $arRes = $el;
		    // Массив для получения городов одним запросом
		    $arCityListId[$el["ID"]] = $el["PROPERTIES"]["CITY"]["VALUE"];
		    // Массив для получения категорий одним запросом
		    $arCatListId[$el["ID"]] = $el["PROPERTIES"]["CATEGORY"]["VALUE"];
		endwhile;

		// Получаем названия городов и добавляем их в массив.
		$arRes["CITY"] = $this->getCity($arCityListId);	
		// Получаем название категории
		$arRes["CAT"] = $this->getCat($arCatListId);

        // Поля текущего юзера
        $rsUser = CUser::GetByID(CUser::GetID());
        $arRes["thisUser"] = $rsUser->Fetch();

        // Если только для PRO можно ли сделать свое предложение
        if($arRes["PROPERTIES"]["PRO"]["VALUE"] == 1){
            if( $arRes["thisUser"]["UF_PRO"] == 1 ){
                $arRes["thisUser"]["add_deals"] = 1;
            }else{
                $arRes["thisUser"]["add_deals"] = 0;
            }
        }else{
            $arRes["thisUser"]["add_deals"] = 1;
        }

        // Если этот же юзер - закрыть форму предложения.
        if($arRes["PROPERTIES"]["USER"]["VALUE"] == $arRes["thisUser"]["ID"] ){
            $arRes["thisUser"]["add_deals"] = 3; // 3 это автор
        }

		return $arRes;
	}

	/**
	 * Проверяем мастера делалал ли он предложения  
	 * @return int;
	 */
	private function checkDealUsr($res, $pro){
  		if(Cuser::IsAuthorized() ){
	  	
			if($pro != 1){
				// Ограничение кол-ва в сутки.
				$date = date(CDatabase::DateFormatToPHP(CSite::GetDateFormat("SHORT")), time() );
				$arFilter = array("IBLOCK_ID"=>$this->arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "ACTIVE_DATE"=>"Y", 
					array(
						"LOGIC" => "AND",
						array("PROPERTY_DEALS" => GPropertyTable::GetColumnFilter("USER", Cuser::GetID() )), 
						array("PROPERTY_DEALS" => GPropertyTable::GetColumnFilter(
							"DATE",
							$date."%",
							Array("USER",
								"DATE",
								"PRICE",
								"PRICE_ROAD", 
								"PREPAY_SERVICE",
								"PREPAY_MATERIALS",
								"TEXT"
								)
							) 
						),
					),
				);
				$arSelect = Array("IBLOCK_ID", "ID"); 
				$items = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );
				$count_deals = $items->SelectedRowsCount();
				// echo "предлож=".$count_deals." ";
				if($this->arParams["DEALS_DEY"] <= $count_deals){
					$arRes["thisUser"]["add_deals"] = 5;
				}
			}else{
				$arRes["thisUser"]["add_deals"] = 1; // pro
			}

			//  Если мастер уже добавлял предложение в этом тендере
  			$this_usr = Cuser::GetID();
			foreach ($res as $arItem) {
				if($arItem["USER"] == $this_usr ){
					$arRes["thisUser"]["add_deals"] = 4; // Вы уже делали предложение
				}	
			}
		}
		return $arRes["thisUser"]["add_deals"];	
	}

	/**
	 * выполняет логику работы компонента
	  * @return array;
	 */
	public function executeComponent(){
	    if($this->startResultCache(false, $this->getThisUrl()) ){
	        $this->arResult = $this->getTender();

		    $this->restoreUrl($this->arResult["CODE"], $this->arResult["ID"]); // если символьный код не правильный то перелинковать.
		    $deals_yes = $this->checkDealUsr($this->arResult["PROPERTIES"]["DEALS"]["VALUE"], $this->arResult["thisUser"]["UF_PRO"]);
		    if( $deals_yes > 0 )
		    	$this->arResult["thisUser"]["add_deals"] = $deals_yes;
		    $this->addDeal($this->arResult);
			$this->arResult["NAV"] = $this->getNav($this->arResult["PROPERTIES"]["DEALS"]["VALUE"]);
	    }

	    $this->includeComponentTemplate();

	    return $this->arResult;
	}
}


?>