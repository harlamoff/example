<?
CModule::AddAutoloadClasses(
	'',
	array(
	   // ключ - имя класса, значение - путь относительно корня сайта к файлу с классом
		'Service' => '/bitrix/php_interface/include/handlers/iblock/service.php',
		'Profile' => '/bitrix/php_interface/include/handlers/main/profile.php',
		'PayOrder' => '/bitrix/php_interface/include/handlers/sale/payorder.php',
		'ModerationMail' => '/bitrix/php_interface/include/handlers/iblock/moderation_mail.php',
		'Review' => '/bitrix/php_interface/include/handlers/iblock/review.php',
		'Subscribe' => '/bitrix/php_interface/include/handlers/iblock/subscribe.php',
	)
);

/* --- Проверка при добавлении и изменении ИБ услуги --- */
	AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("Service", "OnBeforeIBlockElementUpdateHandler"));
	AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("Service", "OnBeforeIBlockElementAddHandler"));
/* --- END Проверка при добавлении и изменении ИБ услуги --- */

/* --- Регистрация по email --- */
	AddEventHandler("main", "OnBeforeUserRegister", Array("Profile", "OnBeforeUserUpdateHandler") );
	AddEventHandler("main", "OnBeforeUserUpdate", Array("Profile", "OnBeforeUserUpdateHandler") );
/* --- END Регистрация по email --- */

/* --- Сжать фото в профиле --- */
	AddEventHandler("main", "OnBeforeUserUpdate", Array("Profile", "UserPhotoResize") );
/* --- END Сжать фото в профиле --- */

/* --- Оплачен PRO, VIP - переключить аккаунт или объявление или тендер --- */
	AddEventHandler("sale", "OnSalePayOrder", Array("PayOrder", "PayPRO") );
/* --- END Оплачен PRO, VIP - переключить аккаунт или объявление или тендер --- */

/* --- Оповещение модератора --- */
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("ModerationMail", "SendMailAdd"));
	AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("ModerationMail", "SendMailUpdate"));
	AddEventHandler("main", "OnBeforeUserUpdate", Array("Profile", "UpdateIdentification") );
/* --- END Оповещение модератора --- */

/* --- Пересчитываем рейтинг отзывов и обновляем поле мастера UF_RATE_SUM --- */
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("Review", "UpdateRateMaster"));
	AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("Review", "UpdateRateMaster"));
	AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("Review", "UpdateRateMaster"));
/* --- END Пересчитываем рейтинг отзывов и обновляем поле мастера UF_RATE_SUM --- */

/* --- Управление подписками пользователя --- */
	// Новое сообщение
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("Subscribe", "NewMess"));
	// Новое событие
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("Subscribe", "NewEvent"));
/* --- END Управление подписками пользователя --- */

?>