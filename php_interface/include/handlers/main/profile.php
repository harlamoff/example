<?
class Profile{

	/**
	 * добавляем в пользователя в группу. Заказчик или исполнитель
	 * @return array
	 */
	public function OnBeforeUserUpdateHandler(&$arFields){
	    if ($arFields["UF_TYPE_ACCOUNT"] == 6 || $arFields["UF_TYPE_ACCOUNT"] == 7 ){
	        $arFields["GROUP_ID"] = array_merge($arFields["GROUP_ID"] , array($arFields["UF_TYPE_ACCOUNT"]) );
	    }
	    $arFields["LOGIN"] = $arFields["EMAIL"];
	    return $arFields;
	}

	/**
	 * Сжимаем изображения аватарки юзера
	 */
	public function UserPhotoResize(&$arFields){
		if(!CModule::IncludeModule("iblock")) exit();

		### внимаение макс размер фото! Зависти от настр PHP.INI
		if($arFields['PERSONAL_PHOTO']['size']>1024*1024*10){
		  GLOBAL $APPLICATION;
		  $APPLICATION->throwException('Максимальный размер фотографии 5 мегабайт');
		  unset($arFields['PERSONAL_PHOTO']);
		  return false;
		}
		elseif($arFields['PERSONAL_PHOTO']){
			$arFields['PERSONAL_PHOTO']["old_file"] = $arFields['PERSONAL_PHOTO']["tmp_name"];
			$arNewFile = CIBlock::ResizePicture($arFields['PERSONAL_PHOTO'], array(
				"WIDTH" => 320,
				"HEIGHT" => 320,
				"METHOD" => "resample",
			));  

		    if($arNewFile){
		    	$arFields['PERSONAL_PHOTO'] = $arNewFile;

		    	// Удаление старой фото пользователя
				$rsUser = CUser::GetByID($arFields["ID"]);
				$oldUser = $rsUser->Fetch();
				if($oldUser["PERSONAL_PHOTO"] > 0){
					$arFields['PERSONAL_PHOTO']["old_file"] = $oldUser["PERSONAL_PHOTO"];
				}
		    }
		    else
		    	$APPLICATION->throwException("Ошибка масштабирования аватарки пользователя в свойстве PERSONAL_PHOTO"); 
		}
	}

	/**
	 * После изменения рейтинга мастера (UF_RATE_SUM) - обновим рейтиг во всех улугах этого мастера
	 * И обновляем заодно PRO 
	 */
	public function UpdateMasterRateService(&$arFields){
		if(!CModule::IncludeModule("iblock")) exit();
		// Если мастер
		if( $arFields["ID"] > 0 && in_array( 7, CUser::GetUserGroup( $arFields["ID"] )) ){

		    $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arFields["ID"]),
		        array("FIELDS"=>array("ID"), "SELECT"=>array("UF_PRO", "UF_RATE_SUM") ));
		    $thisUser = $rsUser->Fetch();

			$field = array();
			if($thisUser["UF_PRO"] == 1){
		   		$field["PRO"] = 31;
			}else{
				$field["PRO"] = "";
			}
			$field["RATE_SUM"] = $thisUser["UF_RATE_SUM"];
			if($thisUser["ID"] > 0){
			   	// Получаем список услуг данного мастера
				$items = CIBlockElement::GetList( array(), array("IBLOCK_ID"=>6, "PROPERTY_USER"=>$thisUser["ID"]), false, false, array("ID", "IBLOCK_ID") );			
				while($el = $items->Fetch()):
					// обновляем все услуги
					CIBlockElement::SetPropertyValuesEx($el["ID"], 6, $field );	
				endwhile;	
			}
		}
	}
	
	/**
	 * Обновляем в профиле прохождение идентификации по паспорту
	 * Если идентификация пройдина отпр. юзеру сообщение
	 */
	public function UpdateIdentification(&$arFields){
		global $APPLICATION, $USER;

		if( $arFields["ID"] > 0 && $arFields["UF_IDENTIFICATION"] == 1 ){
		    $thisUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arFields["ID"]),
		        array("FIELDS"=>array("ID"), "SELECT"=>array("UF_IDENTIFICATION") ))->Fetch();

			if($thisUser["UF_IDENTIFICATION"] != 1){
	    		// Отравка события пользователю
				$APPLICATION->IncludeComponent(
					"harlamoff:events-new",
					"",
					Array(
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_TYPE" => "USER",
						"IBLOCK_ID" => "15",
						"MESSAGE" => '

							Идентификация пройдена успешно<br>
							Спасибо.

						',
						"FROM" => 'Идентификация пройдена успешно :)',
						"TO" => $arFields["ID"],
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "3600"
					)
				);
			}
		}
	}
}

?>